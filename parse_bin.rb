#!/usr/bin/env ruby

# select file
if ARGV[0] == nil then
  puts "usage: #{$0} <binary_log>"
  2.times { puts "" }
  puts "(hint, the log file provided is ---> txnlog.dat)"
  exit(0)
else
  binary_log = ARGV[0].to_s  
end

# read file in ASCII representation
binary = File.open( binary_log, "rb" ) do |file|
  file.read
end

# get header info
header_magic = binary[0..3]
header_version = binary[4]
number_of_records = binary[5..8]

# strip headers from the log
binary = binary[9..-1]

# print header info
puts "" 
puts "Header Magic: #{header_magic}"
puts "Log Version: #{header_version.unpack1('C*')}"
puts "Number of Records: #{number_of_records.unpack1("L>*")}"
puts ""

# print legend 
puts "-----------------------------------------------------------------------"
puts "Record Type  | Unix timestamp | UserID              | Amount in Dollars"
puts "-----------------------------------------------------------------------"

# zero variables
start_pos = end_pos = debit_sum = credit_sum = sa_total = ea_total = balance = uid_check = 0

# loop through log and decode binary data
while start_pos < binary.length 
  record_enum = binary[start_pos].unpack1("U*>")
  
  # shift to timestamp position and decode
  start_pos += 1
  end_pos = start_pos + 3
  timestamp = binary[start_pos..end_pos].unpack1("L>*") 

  # shift user ID position and decode
  start_pos = end_pos + 1
  end_pos = start_pos + 7
  uid = binary[start_pos..end_pos].unpack1("Q>*")

  # if a credit or debit exists, then decode; otherwise, write a zero 
  if record_enum <= 1
    start_pos = end_pos + 1
    end_pos = start_pos + 7
    amount = binary[start_pos..end_pos].unpack1("G*")
  else 
    amount = 0
  end
  start_pos = end_pos + 1
  # set record name, tally amounts, track autopay occurences
  case record_enum
  when 0
    record_type = "Debit       "
    debit_sum += amount
  when 1
    record_type = "Credit      "
    credit_sum += amount
  when 2
    record_type = "StartAutopay"
    sa_total += 1
  when 3
    record_type = "EndAutopay  "
    ea_total += 1
  else
    record_type = "Error: unknown record type: #{record_enum}"
    exit(false)
  end

  # check for specific user ID and get the amount
  if uid == 2456938384156277127 or uid == ARGV.shift.to_i
    balance = amount
    uid_check = uid
  end
  
  # print log info in plain text and adjust formatting as needed
  print "#{record_type} | #{timestamp}     | #{uid} "
  if uid.to_s.length != 19
    (19 - uid.to_s.length).times do
      print "\s"
    end
  end
  puts "| #{amount.round(12)}"
  puts "-----------------------------------------------------------------------"
end

# make it pretty
debit_sum = debit_sum.to_s.insert (debit_sum.to_s.index("\.") - 3), ","
credit_sum = credit_sum.to_s.insert (credit_sum.to_s.index("\.") - 3), ","

# return assignment results
puts ""
puts "total amount in dollars of debits:  $#{debit_sum}"
puts "total amount in dollars of credits: $#{credit_sum}"
puts ""
puts "#{sa_total} autopays were started"
puts "#{ea_total}  autopays were ended"
puts ""
puts "User ID #{uid_check} has a balance of $#{balance}"
